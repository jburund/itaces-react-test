import styled from 'styled-components';
import React, {forwardRef, useImperativeHandle} from 'react';
import ReactDOM from 'react-dom';

const ModalBG = styled.div`
position: absolute;
background: rgba(0, 0, 0, 0.7);
width: 100%;
height: 100%;
display: flex;
justify-content: center;
align-items: center;
z-index: 100;
`;

const ModalWindow = styled.div`
background: #FFFFFF;
border: 1px solid rgba(0, 0, 0, 0.05);
box-sizing: border-box;
box-shadow: 0px 6px 10px rgba(0, 0, 0, 0.1);
border-radius: 5px;
padding: 20px;
margin:30px;
z-index: 101;
`;

const ModalMessage = styled.p`
font-family: Gotham Pro;
font-style: normal;
font-weight: normal;
font-size: 12px;
line-height: 125%;
/* or 15px */
color: #000000;
margin: 0 0 5px 0;
`;

const ModalCloseIcon = styled.svg`
height: 24px;
width: 24px;
transition: fill 0.2s;

  &:hover{
    fill: #60474D;
  }
`;

const ModalHeader = styled.div`
display: flex;
justify-content: space-between;
align-items: center;
font-family: Gotham Pro;
font-style: normal;
font-weight: bold;
font-size: 18px;
line-height: 125%;
color: #60474D;
padding-bottom: 24px;
`;

const Modal = forwardRef((props,ref) => {

    const [display, setDisplay] = React.useState(false);

    useImperativeHandle(ref, () => {
       return {
        openModal: () => open(),
        close: () => close()
       }
    });

    const open = () => {
        setDisplay(true);
    };

    const close = () => {
        setDisplay(false);
    }

    if (display) {
        return ReactDOM.createPortal(
            <ModalBG>
                <ModalWindow>
                    <ModalHeader>{props.title}
                        <ModalCloseIcon onClick={close} viewBox="0 0 24 24" fill="rgba(0, 0, 0, 0.5)">
                            <path d="M19 6.41L17.59 5L12 10.59L6.41 5L5 6.41L10.59 12L5 17.59L6.41 19L12 13.41L17.59 19L19 17.59L13.41 12L19 6.41Z"/>
                        </ModalCloseIcon>
                    </ModalHeader>
                    {props.children}
                </ModalWindow>
            </ModalBG>
            , document.getElementById('modal-root'))
    }

    return null;

});

export  {ModalBG, ModalWindow, ModalMessage};
export default Modal;
