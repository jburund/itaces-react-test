import styled from 'styled-components';

const Form = styled.div`
  display: flex;
  flex-direction: column;
  margin: auto;
  width: 440px;
  padding:20px;
`;

const Input = styled.input`
    height: 38px;
    width: calc(100% - 2px);
    background-color: #ffffff;
    border-radius: 5px;
    border: 1px solid rgba(0, 0, 0, 0.05);
    text-indent: 87px; // Arbitrary.
    transition: all .3s ease-in-out;
    /*font*/
    color: rgba(0, 0, 0, 0.5);
    line-height: 1.25;
    text-align: left;
    font-size: 12px;
    font-family: GothamPro;
    font-weight: 400;
    margin: 0 0 18px 0;
    padding: 0;
`;

const SecondPassInput = styled.input`
    height: 38px;
    width: calc(100% - 2px);
    background-color: #ffffff;
    border-radius: 5px;
    border: 1px solid rgba(0, 0, 0, 0.05);
    text-indent: 130px; // Arbitrary.
    transition: all .3s ease-in-out;
    /*font*/
    color: rgba(0, 0, 0, 0.5);
    line-height: 1.25;
    text-align: left;
    font-size: 12px;
    font-family: GothamPro;
    font-weight: 400;
    margin: 0 0 18px 0;
    padding: 0;
`;


const InputLabel = styled.label`
    color: rgba(0, 0, 0, 0.7);
    line-height: 1.25;
    text-align: left;
    font-size: 12px;
    font-family: GothamPro;
    font-weight: 700;
    display: inline-block;
    position: absolute;
    top:0;
    left:0;
    padding-left: 12px;
    padding-top: 13px;
`;

const Button = styled.button`
  height: 40px;
  width: 100%;
  background: url(../img/button.svg);
  border:0;
  //FONT
  color: #ffffff;
  line-height: 1.25;
  text-align: center;
  font-size: 12px;
  font-family: GothamPro;
  font-weight: 700;
  
  &:hover {
    //outline: none !important;
  }
`;

const Container = styled.div`
  //background: linear-gradient(to bottom, #6371c7, #5563c1);
  width: 100%;
  height: 100vh;
  background: url(../img/back.jpg);
  display: flex;
`;

const FormLink = styled.div`
    margin-bottom: 16px;
    padding-bottom: 2px;
    border-bottom: 1px solid white;
    color:white;
    font-family: Gotham Pro;
    font-style: normal;
    font-weight: normal;
    font-size: 12px;
    line-height: 130%;
    display: inline-block;
`;

const LoginTitle = styled.h1`
    font-family: Gotham Pro;
    font-style: normal;
    font-weight: bold;
    font-size: 12px;
    line-height: 125%;
    text-align: center;
    color: #FFFFFF;
    margin-bottom: 23px;
`;

const InputSpan = styled.span`
    position:relative;
`;

export { Form, FormLink, Input, Button, Container, LoginTitle, InputLabel, InputSpan, SecondPassInput};