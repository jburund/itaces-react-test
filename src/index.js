import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, Route } from "react-router-dom";

import ShowRegPage from "./pages/Auth/registration"
import ShowAuthPage from "./pages/Auth";

ReactDOM.render(
  <React.StrictMode>
      <BrowserRouter>
                  <Route path='/registration' component={ShowRegPage}/>
                  <Route path='/' component={ShowAuthPage}/>
      </BrowserRouter>
  </React.StrictMode>,
  document.getElementById('root')
);

