import React, {forwardRef, useImperativeHandle, useState} from 'react';
import {Input, Button, Container, LoginTitle, InputLabel, InputSpan, Form, FormLink, SecondPassInput} from "../../components/AuthForm";
import {ModalMessage} from "../../components/ModalWindow";
import Modal from "../../components/ModalWindow";
import {Link} from "react-router-dom";

export default function ShowRegPage() {

    const [modaltitle, setModaltitle] = React.useState('');
    const [username, setUsername] = React.useState('');
    const [password, setPassword] = React.useState('');
    const [secpassword, setSecpassword] = React.useState('');
    const [Message, setMessage] = React.useState('');


    const modalRef = React.useRef();

    const openModal = () => {
        console.log('Имя: ' + username);
        console.log('Пароль: ' + password);
        if (username == '') {
            modalRef.current.openModal();
            setModaltitle('Провал');
            setMessage('Введите имя пользователя');
        } else if (username.length < 3) {
            modalRef.current.openModal();
            setModaltitle('Провал');
            setMessage('Слишком короткое имя пользователя');}
        else if (password == ''){
            modalRef.current.openModal();
            setModaltitle('Провал');
            setMessage('Введите пароль');}
        else if (password.length < 4){
            modalRef.current.openModal();
            setModaltitle('Провал');
            setMessage('Слишком короткий пароль!');}
        else if (password != secpassword) {
            modalRef.current.openModal();
            setModaltitle('Провал');
            setMessage('Пароли не совпадают!');}
        else {
            modalRef.current.openModal();
            setModaltitle('Успех');
            setMessage(`Пользователь ${username} успешно зарегистрирован.`);
        }
        // console.log(username);
    };


    return(

        <Container>

            <Form>
                <LoginTitle>
                    Регистрация
                </LoginTitle>
                <InputSpan>
                    <Input placeholder={'Ваш логин'} title={'Логин'} id="LoginInput" onChange={(e) => setUsername(e.target.value)} value={username}/>
                    <InputLabel for="LoginInput">Логин:</InputLabel>
                </InputSpan>
                <InputSpan>
                    <Input placeholder={'Ваш пароль'} title={'Пароль'} type={'password'} onChange={(e) => setPassword(e.target.value)} id="PasswordInput" value={password}/>
                    <InputLabel for="PasswordInput">Пароль:</InputLabel>
                </InputSpan>
                <InputSpan>
                    <SecondPassInput placeholder={'Пароль ещё раз'} title={'Повтор пароля'} type={'password'} onChange={(e) => setSecpassword(e.target.value)} id="SecondPasswordInput" value={secpassword}/>
                    <InputLabel for="SecondPasswordInput">Повтор пароля:</InputLabel>
                </InputSpan>
                <Link to="/" style={{ textDecoration: 'none',marginTop: '-5px' }}><FormLink>Логин</FormLink></Link>
                <Button onClick={openModal}>Регистрация</Button>

            </Form>
            <Modal title={modaltitle} ref={modalRef}>
                <ModalMessage>{Message}</ModalMessage>
            </Modal>
        </Container>
    )
}
