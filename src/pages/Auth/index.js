import React, {forwardRef, useImperativeHandle, useState} from 'react';
import {Input, Button, Container, LoginTitle, InputLabel, InputSpan, Form, FormLink } from "../../components/AuthForm";
import {ModalMessage} from "../../components/ModalWindow";
import Modal from "../../components/ModalWindow";
import { Link } from "react-router-dom";

export default function ShowAuthPage() {



    const [modaltitle, setModaltitle] = React.useState('');
    const [username, setUsername] = React.useState('');
    const [password, setPassword] = React.useState('');
    const [Message, setMessage] = React.useState('');


    const modalRef = React.useRef();

    const openModal = () => {
    console.log('Имя: ' + username);
    console.log('Пароль: ' + password);
        if (username == '') {
            modalRef.current.openModal();
            setModaltitle('Провал');
            setMessage('Введите имя пользователя');
        } else if (username.length < 3) {
            modalRef.current.openModal();
            setModaltitle('Провал');
            setMessage('Слишком короткое имя пользователя');}
        else if (password == ''){
            modalRef.current.openModal();
            setModaltitle('Провал');
            setMessage('Введите пароль');}
        else if (password.length < 4){
            modalRef.current.openModal();
            setModaltitle('Провал');
            setMessage('Слишком короткий пароль!');}
        else {
            modalRef.current.openModal();
            setModaltitle('Успех');
            setMessage(`Пользователь ${username} успешно вошел в систему.`);
        }
        // console.log(username);
    };



    return(

        <Container>

            <Form>
                <LoginTitle>
                    Логин
                </LoginTitle>
                <InputSpan>
                    <Input placeholder={'Ваш логин'} title={'Логин'} id="LoginInput" onChange={(e) => setUsername(e.target.value)} value={username}/>
                    <InputLabel for="LoginInput">Логин:</InputLabel>
                </InputSpan>
                <InputSpan>
                    <Input placeholder={'Ваш пароль'} title={'Пароль'} type={'password'} onChange={(e) => setPassword(e.target.value)} id="PasswordInput" value={password}/>
                        <InputLabel for="PasswordInput">Пароль:</InputLabel>
                </InputSpan>
                <Link to="/registration" style={{ textDecoration: 'none',marginTop: '-5px' }}><FormLink>Регистрация</FormLink></Link>
                <Button onClick={openModal}>Войти</Button>

            </Form>
            <Modal title={modaltitle} ref={modalRef}>
                <ModalMessage>{Message}</ModalMessage>
            </Modal>
        </Container>
    )
}
